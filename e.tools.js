/*
 * Capitalize Ecommerce Tools
 * Created By Rafael Cruz
 * Contact: rafaeldriveme@gmail.com
 * Version: 0.0.1
 * Release: 03/05/2015
 * Ecommerce plugins
 */
(function($) {
    'use strict';

    /* Privados */

    // Apenas numeros

    function apenasNumeros(string) {
        var numsStr = string.replace(/[^0-9]/g, '');
        return parseInt(numsStr);
    }


    // Valida CPF
    function CPF() {

          "user_strict";

          function r(r) {
            for (var t = null, n = 0; 9 > n; ++n) t += r.toString().charAt(n) * (10 - n);
            var i = t % 11;
            return i = 2 > i ? 0 : 11 - i
          }

          function t(r) {
            for (var t = null, n = 0; 10 > n; ++n) t += r.toString().charAt(n) * (11 - n);
            var i = t % 11;
            return i = 2 > i ? 0 : 11 - i
          }

          var n = false,
              i = true;

          this.gera = function() {
            for (var n = "", i = 0; 9 > i; ++i) n += Math.floor(9 * Math.random()) + "";
            var o = r(n),
              a = n + "-" + o + t(n + "" + o);
            return a
          }, this.valida = function(o) {
            for (var a = o.replace(/\D/g, ""), u = a.substring(0, 9), f = a.substring(9, 11), v = 0; 10 > v; v++)
              if ("" + u + f == "" + v + v + v + v + v + v + v + v + v + v + v) return n;
            var c = r(u),
              e = t(u + "" + c);
            return f.toString() === c.toString() + e.toString() ? i : n

          }
    }

    var CPF = new CPF();

    // Para o formato monetário
    function number_format(number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(',', '').replace(' ', '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    // Transforma textos e palavras no formato rewrite
    function strToURL(str) {
        return str.toLowerCase().trim()
            .replace(/[áàãâä]/g, "a")
            .replace(/[éèẽêë]/g, "e")
            .replace(/[íìĩîï]/g, "i")
            .replace(/[óòõôö]/g, "o")
            .replace(/[úùũûü]/g, "u")
            .replace(/ç/g, "c")
            .replace(/(\ |_)+/, " ")
            .replace(/(^-+|-+$)/, "")
            .replace(/[^a-z0-9]+/g, '-');
    }

    // Pega parametros via url
    function getParameterByName(name, href) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(href);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    //Verifica se o email é valido ou não
    function validEmail(email) {
        var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        if (filtro.test(email)) {

            return true;

        } else {

            return false;

        }
    }

    // Valida sua url
    function validURL(url) {
        var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
        return regexp.test(url);
    }

    //**** CONVERSÃO DE DINHEIRO PARA FLOAT ****/
    function convertMoneyToFloat(vl, symbol) {
        return parseFloat(vl.replace(".", "").replace(",", ".").replace(symbol, "").replace(symbol + " ", ""));
    }

    //**** PEGAR VALOR DO BOLETO ****//
    function getValueBill(valor_produto, desc_boleto, symbol) {

        var vl = [];
        var valor_produto, desc_boleto, vl_product, valor_desc;

        if (valor_produto && desc_boleto) {

            vl_product = valor_produto;

            valor_desc = (valor_produto * desc_boleto) / 100;
            valor_produto = valor_produto - valor_desc;
            valor_produto = number_format(valor_produto, 2, ',', '.');
            valor_produto = symbol + " " + valor_produto;

            valor_desc = number_format(valor_desc, 2, ',', '.');
            valor_desc = symbol + " " + valor_desc;

            vl['total'] = valor_produto;
            vl['descount'] = valor_desc;
            vl['porcent_descount'] = desc_boleto;

            return vl;
        }

    }

    //***PEGAR VALOR DO parcelamneto**//
    function parcelamento(vl, parcmax, juros, valmax, parcs, symbol) {
        //valor sem juros

        var val = [];
        var parcs = vl / valmax;
        var parcs = parseInt(parcs);
        var preco_produto;
        var I = "";

        if (parcs > parcmax) {
            parcs = parcmax;
        }

        preco_produto = vl / parcs;

        if (juros == 0) {

            if (parcs > 0) {

                val['parc'] = parcs + "x";
                val['vl'] = symbol + " " + number_format(preco_produto, 2, ',', '.');
                val['juros'] = null;

                return val;

            } else {
                return false;
            }

        } else {

            //valor com juros
            I = juros / 100;
            preco_produto = (vl * Math.pow((1 + I), parcs)) / parcs;

            if (parcs > 0) {

                val['parc'] = parcs + "x";
                val['vl'] = symbol + " " + number_format(preco_produto, 2, ',', '.');
                val['juros'] = parseFloat(juros) + "% a.m";

                return val;

            } else {
                return false;
            }

        }

    }

    //*** PEGAR VALOR DO FINANCIAMNETO **//
    function financiamento(vl, parcmax, juros, valmax, parcs, symbol) {
        //valor sem juros

        var val = [];
        var parcs = vl / valmax;
        var parcs = parseInt(parcs);
        var preco_produto;
        var I = "";

        if (parcs > parcmax) {
            parcs = parcmax;
        }

        preco_produto = vl / parcs;

        if (juros) {

            //valor com juros
            I = juros / 100;
            preco_produto = (vl * I * Math.pow((1 + I), parcs)) / (Math.pow((1 + I), parcs) - 1);

            if (parcs > 0) {

                val['parc'] = parcs + "x";
                val['vl'] = symbol + " " + number_format(preco_produto, 2, ',', '.');
                val['juros'] = parseFloat(juros) + "% a.m";

                return val;

            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    /* Setar um Cookie no Navegador */
    function sCookie(cname, cvalue, horas, domain) {
        var d = new Date();
        d.setTime(d.getTime() + (horas * 1000 * 60 * 60));
        var expires = "expires=" + d.toGMTString();
        var path = "path=/";
        var domain = "domain=" + domain;
        document.cookie = cname + "=" + cvalue + ";" + expires + ";" + domain + ";" + path + ";";
        return true;
    }


    /* Pegar Cookie(s) no Navegador */
    function gCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
        }
        return "";
    }

    // Deixar as primeiras letras de cada palavra maiuscula
    function formatName(input) {
        var listaDeNomes = input.split(" ");
        var listaNomeFormatada = listaDeNomes.map(function(nome) {

            if (/(da|de|dos|do)/.test(nome)) return nome;
            return nome.charAt(0).toUpperCase() + nome.substring(1).toLowerCase();

        });
        return listaNomeFormatada.join(" ");
    }

    // Pegar a Data e hora atual
    function date(x) {
        var d = new Date();
        if (x == "d") {
            return ((d.getDate() < 10) ? "0" + (d.getDate()) : d.getDate()) + "/" + ((d.getMonth() + 1 < 10) ? "0" + (d.getMonth() + 1) : d.getMonth() + 1) + "/" + d.getFullYear();
        } else if (x == "h") {
            return ((d.getHours() < 10) ? "0" + d.getHours() : d.getHours()) + ":" + ((d.getMinutes() < 10) ? "0" + (d.getMinutes()) : d.getMinutes()) + ":" + ((d.getSeconds() < 10) ? "0" + (d.getSeconds()) : d.getSeconds());
        }
    }

    // Pegar porcentagem
    function percent(oldValue, newValue) {
        var x = [];
        x['discount'] = Math.abs(parseInt((((newValue / oldValue) * 100) - 100)));
        x['total'] = Math.abs(parseInt((((newValue / oldValue) * 100))));
        x['discount_format'] = x['discount'] + "%";
        x['total_format'] = x['total'] + "%";
        return x;
    }

    // Pegar desconto da porcentagem
    function discountPercent(percent, value, symbol) {
        var x = [];
        x['discount_price'] = (percent / 100) * value;
        x['total_price_with_discount'] = value - ((percent / 100) * value);
        x['total_price_with_increase'] = value + ((percent / 100) * value);
        x['discount_price_format'] = symbol + " " + number_format(x['discount_price'], 2, ',', '.');
        x['total_price_with_discount_format'] = symbol + " " + number_format(x['total_price_with_discount'], 2, ',', '.');
        x['total_price_with_increase_format'] = symbol + " " + number_format(x['total_price_with_increase'], 2, ',', '.');
        return x;
    }

    //Mascara de formulario
    var onlyNumbers = /[^0-9]+/g;
    var date;
    var phone;
    var cep;
    var txt;

    function onlyNumbersConvert(selector) {

        $(selector).keyup(function() {
            txt = $(this).val().replace(onlyNumbers, "");
            $(this).val(txt);
        });

    }

    function maskForm(selector, type) {

        if (type == "date") {

            $(selector).keyup(function() {
                date = $(this).val().replace(onlyNumbers, "");

                if (date.length > 2) {
                    date = date.substring(0, 2) + "/" + date.substring(2)
                }

                if (date.length > 5) {
                    date = date.substring(0, 5) + "/" + date.substring(5, 9)
                }

                $(this).val(date);

            });

        } else if (type == "phone") {

            $(selector).keyup(function() {

                phone = $(this).val().replace(onlyNumbers, "");

                if (phone.length > 2) {
                    phone = "(" + phone.substring(0, 2) + ") " + phone.substring(2)
                }

                if (phone.length > 9) {
                    phone = phone.substring(0, 9) + "-" + phone.substring(9, 14)
                }

                $(this).val(phone);

            });

        } else if (type == "CEP") {

            //99999-999

            $(selector).keyup(function() {

                cep = $(this).val().replace(onlyNumbers, "");

                if (cep.length > 5) {
                    cep = cep.substring(0, 5) + "-" + cep.substring(5, 8);
                }

                $(this).val(cep);

            });

        }

    }

    function validacao(selector) {

        $("body").delegate(selector, "submit", function() {

            var errors = "";
            var name_label = "";
            var result = [];

            $(this).find(".error").remove();
            $(this).find(".required_input").removeClass("valid");
            $(this).find(".required_input").removeClass("invalid");

            // Valida
            $(this).find(".required_input").each(function() {

                if ($(this).val()) {

                    if ($(this).hasClass("valid")) {
                        $(this).addClass("valid");
                    }

                    if (errors.indexOf(",#" + $(this).attr("id")) > -1) {
                        errors += errors.replace(",#" + $(this).attr("id"), "");
                        name_label += errors.replace("," + $(this).parent(".form-row").find("label").attr("rel"), "");
                    }

                    if($(this).attr('name') == 'email'){

                        console.log(validEmail($(this).val()))

                        if (!validEmail($(this).val())) {

                            // email invalido

                            if (errors.indexOf(",#" + $(this).attr("id")) <= -1) {
                                errors += ",#" + $(this).attr("id");
                                name_label += "," + $(this).parents(".form-row").find("label").attr("rel");
                                alert("Seu email é inválido!");
                            }

                        }else{

                             if (errors.indexOf(",#" + $(this).attr("id")) > -1) {
                                errors += errors.replace(",#" + $(this).attr("id"), "");
                                name_label += errors.replace("," + $(this).parent(".form-row").find("label").attr("rel"), "");
                             }

                        }

                    }

                    if($(this).attr('name') == 'cpf'){

                        if (!CPF.valida($(this).val())) {

                            // cpf invalido

                            if (errors.indexOf(",#" + $(this).attr("id")) <= -1) {
                                errors += ",#" + $(this).attr("id");
                                name_label += "," + $(this).parents(".form-row").find("label").attr("rel");
                                alert("CPF inválido!");
                            }

                        }else{

                             if (errors.indexOf(",#" + $(this).attr("id")) > -1) {
                                errors += errors.replace(",#" + $(this).attr("id"), "");
                                name_label += errors.replace("," + $(this).parent(".form-row").find("label").attr("rel"), "");
                             }

                        }

                    }               

                } else {

                    if (errors.indexOf(",#" + $(this).attr("id")) <= -1) {
                        errors += ",#" + $(this).attr("id");
                        name_label += "," + $(this).parents(".form-row").find("label").attr("rel");
                    }

                }

                console.log("Erros:");
                console.log(errors);

            });

            var err = errors.split(",");
            err.splice(err[0], 1);

            var nam = name_label.split(",");
            nam.splice(nam[0], 1);

            if (err.length > 0) {

                var count = 0;
                err.forEach(function(id) { //Iterar um array
                    $("<span class='error'>Informe " + nam[count++].toLowerCase() + "</error>").insertAfter($(id));
                    $(id).addClass("invalid");
                })

                $(this).removeClass("form-sucess");
                $(this).addClass("form-error");

                return false;

            } else {

                $(this).addClass("form-sucess");
                $(this).removeClass("form-error");

                return false;


            }

        });

        // Validar Campo obrigatorios no blur

        //$(selector).find(".required_input").not("#email").blur(function() {
         $("body").delegate(".required_input", "blur", function() {

            var _this = $(this);

            setTimeout(function(){

                _this.next(".error").remove();

                if (_this.val()) {

                    // Padrão para validar a existencia de valor

                    _this.removeClass("invalid");
                    _this.addClass("valid");
                    _this.next('.error').hide();

                    // Padrão para validar a existencia de valor

                    // campo valido

                    if(_this.attr('name') == 'email'){

                        console.log(validEmail(_this.val()))

                        if (validEmail(_this.val())) {

                            // email valido
                            _this.removeClass("invalid");
                            _this.addClass("valid");
                            _this.next('.error').hide();
                          

                        }else{

                             // email invalido
                             _this.removeClass("valid");
                             _this.addClass("invalid");
                             _this.next('.error').show();
                             alert("Seu email é inválido!");

                        }

                    }


                    if(_this.attr('name') == 'cpf'){

                        if (CPF.valida(_this.val())) {

                            // cpf valido
                            _this.removeClass("invalid");
                            _this.addClass("valid");
                            _this.next('.error').hide();
                            

                        }else{

                             // cpf invalido
                             _this.removeClass("valid");
                             _this.addClass("invalid");
                             _this.next('.error').show();
                             alert("CPF inválido!");

                        }

                    }

                
                } else {

                    _this.removeClass("valid");
                    _this.addClass("invalid");
                    _this.next('.error').show();

                    $("<span class='error'>Campo obrigatório</error>").insertAfter(_this);

                }

            },1000);

        });

    }

    function getLocation() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
         var l = [];
         l[0] = position.coords.latitude;
         l[1] = position.coords.longitude; 

         return l;
    }

    function userLocalization() {
        //getLocation();
        $.ajax({
            url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&sensor=true',
            success: function(data) {
                console.log(data.results[0].formatted_address);
                /*or you could iterate the components for only the city and state*/
            }
        });
    }

    /* Privados */

    /* Publicos */

    $.fn.etools = function(parameters) {

    };

    // Pega as informações da url
    $.fn.etools.url = function(type_nav) {

        return window.location;

    }

    // Reescreve o texto ou palavra para o formato para url
    $.fn.etools.rewrite = function(str) {

        return strToURL(str);

    }

    // Limitar quantidade de letras
    $.fn.etools.limit_letter = function(str, first_qtd, max_qtd) {

        var pontos = "...";

        if (str.length > max_qtd) {
            return str.substring(first_qtd, max_qtd) + pontos;
        } else {
            return str.substring(first_qtd, max_qtd);
        }

    }

    // Limitar quantidade de palavras
    $.fn.etools.limit_word = function(str, max_qtd) {

        var pontos = "...";
        var word = str.split(" ");
        var words = "";

        if (max_qtd < word.length) {

            for (var cont = 0; cont <= (max_qtd - 1); cont++) {
                words += word[cont] + " ";
            }

            return words + pontos;

        } else {
            return str;
        }

    }

    // Pega parametros via url
    $.fn.etools.getParameter = function(name, href) {

        return getParameterByName(name, href);

    }

    // Verifica se o email é valido
    $.fn.etools.isEmail = function(email) {

        return validEmail(email);

    }

    // Verifica se é uma URL valida
    $.fn.etools.isURL = function(url) {

        return validURL(url);

    }

    // Coverte formato de dinheiro para float
    $.fn.etools.moneyToFloat = function(vl, symbol) {

        return convertMoneyToFloat(vl, symbol);

    }

    // Pegar o valor do desconto do boleto
    $.fn.etools.bill = function(vl, descount, symbol) {

        return getValueBill(convertMoneyToFloat(vl, symbol), parseInt(descount), symbol);

    }

    // Pegar o valor do parcelamento
    $.fn.etools.subdivision = function(vl, parcmax, juros, valmin, parcs, symbol) {

        return parcelamento(convertMoneyToFloat(vl, symbol), parseInt(parcmax), parseFloat(juros), parseFloat(valmin), parseInt(parcs), symbol);

    }

    // Pegar o valor do financiamento
    $.fn.etools.financing = function(vl, parcmax, juros, valmin, parcs, symbol) {

        return financiamento(convertMoneyToFloat(vl, symbol), parseInt(parcmax), parseFloat(juros), parseFloat(valmin), parseInt(parcs), symbol);

    }

    // Setar Cookie
    $.fn.etools.setCookie = function(cname, cvalue, horas, domain) {

        return sCookie(cname, cvalue, horas, domain);

    }

    // Pegar Cookie
    $.fn.etools.getCookie = function(cname) {

        return gCookie(cname)

    }

    // Deixar tudo Maiusculo
    $.fn.etools.toUpper = function(cname) {

        return cname.toUpperCase();

    }

    // Deixar tudo Minusculo
    $.fn.etools.toLower = function(cname) {

        return cname.toLowerCase();

    }

    //Redirecionar página
    $.fn.etools.redirect = function(url) {

        window.location.href = url;

    }

    //Voltar página
    $.fn.etools.backPage = function() {

        window.history.back();

    }

    //Arredondar decimais do float
    $.fn.etools.floatFixed = function(vl) {

        return parseFloat(vl).toFixed(2);

    }

    //Formato monetário
    $.fn.etools.formatMoney = function(number, decimals, dec_point, thousands_sep, symbol) {

        return symbol + number_format(number, decimals, dec_point, thousands_sep);

    }

    //Voltar para o elemento
    $.fn.etools.backTo = function(element, time) {

        $('html,body').animate({
            scrollTop: $(element).offset().top - 100
        }, time);

    }

    //Retorna um numero aleatorio entre x e y
    $.fn.etools.rand = function(from, to) {

        return Math.floor((Math.random() * parseInt(to)) + parseInt(from));

    }

    //Retorna a url corrente
    $.fn.etools.current_url = function() {

        return this.url()['href'];

    }

    //Retorna a url de origem
    $.fn.etools.origin_url = function() {

        return this.url()['origin'];

    }

    // Deixar as primeiras letras de cada palavra maiuscula
    $.fn.etools.format_name = function(name) {

        return formatName(name);

    }

    // Pegar a data e hora corrente
    $.fn.etools.current_date = function(type) {

        return date(type);

    }

    // Porcentagem
    $.fn.etools.getPercent = function(oldValue, newValue) {

        return percent(oldValue, newValue);

    }

    // Porcentagem do desconto
    $.fn.etools.getDiscountPercent = function(percent, value, symbol) {

        return discountPercent(percent, value, symbol);

    }

    // Porcentagem do desconto
    $.fn.etools.mask = function(selector, type) {

        return maskForm(selector, type);

    }

    // Porcentagem do desconto
    $.fn.etools.onlyNumber = function(selector) {

        return onlyNumbersConvert(selector);

    }

    // validar formulario
    $.fn.etools.validate = function(selector) {

        return validacao(selector);

    }

    // Localização do usuario
    $.fn.etools.user_localization = function(){
        return userLocalization();
    }



    /* Publicos */

    /* Propriedades default global */

})($);