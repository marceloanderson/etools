module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            'e.tools.min.js': 'e.tools.js',
        }
    });

    //Carregar modulo
    grunt.loadNpmTasks('grunt-contrib-uglify'); // Minifica arquivos

    // Do
    grunt.registerTask('default', ['uglify']);

}