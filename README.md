# Ecommerce Tools - Plugin JQuery #

Plugin de funções básicas utilizadas na construção de lojas virtuais Vtex e Bseller

### Minificar e ofuscar códigos JS ###

https://www.javascriptobfuscator.com/Javascript-Obfuscator.aspx

### O que ele pode fazer? ###

* Pega informações da url
* Reescreve o texto ou palavra para o formato para url
* Limitar letras
* Pegar parametros da url
* Valida E-mail
* Valida URL
* Converte o formato monetário (R$ 2.377,50) para 2377.5
* Pegar o valor do desconto do boleto
* Pegar o valor do parcelamento
* Pegar Cookie
* Setar Cookie
* Redirecionar
* Converter maiúsculas e minusculas
* Retornar para
* Arrendondar casas decimais do float
* Random
* Limitar palavras
* Formato monetário (de 2377.5 para R$ R$ 2.377,50)
* Pegar endereço da loja
* Pegar a URL corrente da página
* Deixar as primeiras letras de cada palavra maiuscula
* Pegar o valor do financiameto
* Pegar data e hora corrente
* Pegar porcentagem
* Pegar desconto ou inclusão por porcentagem


### Como usar? ###

* Insira no seu <head></head>
* <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
* **<script type="text/javascript" language="javascript" src="e.tools.min.js"></script>**

### Inicialização ###

```
#!javascript

<script>
$(function(){
            // Inicialização do plugin      
            var $tool = $('body').etools;
            // Inicialização do plugin   
        });
</script>
```

### Utilização ###

```
#!javascript

            // Pega informações da url (url corrente, protocolo e etc) 
            $tool.url()["href"]; // dominio

            // Reescreve o texto ou palavra para o formato para url
            $tool.rewrite("Hello word"); // hello-word

            // Limitar quantidade de caracter nos textos
            $tool.substr("Hello word", 0, 5); // Hello...
```


# Ações #

## url() ##
Pega informações da url (url corrente, protocolo e etc) 

```
#!javascript

hash: ""
host: "santamonicanovo.bseller.com.br"
hostname: "santamonicanovo.bseller.com.br"
href: "http://santamonicanovo.bseller.com.br/"
origin: "http://santamonicanovo.bseller.com.br"
pathname: "/"
port: ""
protocol: "http:"
```
### Ex: ###

```
#!javascript

$tool.url()["href"]; 
// http://santamonicanovo.bseller.com.br/
```

## rewrite(txt) ##
Reescreve o texto ou palavra para o formato url
### Ex: ###

```
#!javascript

$tool.rewrite("Hello word");
// hello-word
```

## limit_letter(txt) e limit_word(txt) ##
Limitar quantidade de palavras e letras
### Ex: ###

```
#!javascript

$tool.limit_word("Hello word today now", 2); // Hello word ...

$tool.limit_letter("Hello word today now", 0, 5); // Hello...

```

## getParameter(nome da variavel, url) ##
Pega parametros via url
### Ex: ###

```
#!javascript

$tool.getParameter("v", $tool.url()["href"]); //123

//Ex: http://www.meusite.com?v=123
```

## isEmail(email) ##
Valida seu email (retorna true ou false)
### Ex: ###

```
#!javascript

$tool.isEmail("rafael@stamonica.org");

```

## isURL(url) ##
Valida sua URL (retorna true ou false)
### Ex: ###

```
#!javascript

$tool.isURL("http://www.bing.com.b");

```

## moneyToFloat(numero) ##
Converte o formato monetário (R$ 2.377,50) para 2377.5
### Ex: ###

```
#!javascript

$tool.moneyToFloat("R$ 2.377,50", "R$"); // 2377.5

```

## bill(valor_produto, desc_boleto, simbolo) ##
Pegar o valor do desconto do boleto
### Ex: ###

```
#!javascript

$tool.bill("R$ 2.377,50", 10, "R$")['descount']; //R$ 237,75

//Mais
//[total: "R$ 2.139,75", descount: "R$ 237,75", porcent_descount: 10]

```

## subdivision(valor, parcela máxima, juros, valor minimo, parcela, simbolo) ##
Pegar o valor do parcelamento
### Ex: ###


  Valor de: R$ 377,50

  máx: 12x

  c/ juros de 0%

  c/ o valor minimo de R$ 50,00 reais por parcela


```
#!javascript

$tool.subdivision("R$ 377,50", 10, 0, 50, 10, "R$");
//[parc: "7x", vl: "R$ 53,93", juros: null]

```

## setCookie(nome, valor, horas) ##
Setar o valor do cookie 
### Ex: ###

```
#!javascript

$tool.setCookie("teste_cookie","teste", 2);

```

## getCookie(nome) ##
Pegar o valor do cookie 
### Ex: ###

```
#!javascript

$tool.getCookie("teste_cookie");

```

## toUpper(txt) ##
Deixar tudo Maiúsculo
### Ex: ###

```
#!javascript

 $tool.toUpper("Hello word today now"); //HELLO ...

```

## toLower(txt) ##
Deixar tudo Minusculo
### Ex: ###

```
#!javascript

 $tool.toLower("Hello word today now"); //hello ...

```

## floatFixed(numero) ##
Arredondar os decimais do float
### Ex: ###

```
#!javascript

$tool.floatFixed("2000.911111111111"); // 2000.91

```

## formatMoney(numero) ##
Formato monetário
### Ex: ###

```
#!javascript

$tool.formatMoney("2000.91", 2, ",", ".", "R$"); // R$ 2.000,91

```

## rand(inicio, final) ##
Retorna um numero aleatório entre 1 e 2500
### Ex: ###

```
#!javascript

$tool.rand(1, 2500);

```

## current_url() ##
Retorna a url corrente
### Ex: ###

```
#!javascript

$tool.current_url();

```

## origin_url(); ##
Retorna a url de origem
### Ex: ###

```
#!javascript

$tool.origin_url();

```

## format_name(); ##
Deixar as primeiras letras de cada palavra maiuscula
### Ex: ###

```
#!javascript

$tool.format_name("carlos dos assis"); // Carlos dos Assis

```

## financing(); ##
Financiamento com juros (vl, parcls, juros, parc_min, parcls, symbol)
### Ex: ###

```
#!javascript

$tool.financing("R$ 500,00", 10, 2, 50, 10, "R$");

```

## redirect(); ##
Redirecionar para outra página
### Ex: ###

```
#!javascript

$tool.redirect("/");

```

## current_date(); ##
Pega a data e hora corrente
### Ex: ###

```
#!javascript

$tool.current_date("h"); // Hora
$tool.current_date("d"); // Data

```

## getPercent(oldValue, newValue) ##
// Porcentagem
### Ex: ###

```
#!javascript

$tool.getPercent($tool.moneyToFloat("R$ 100,00", "R$"), $tool.moneyToFloat("R$ 60,00", "R$"))

// [discount: 40, total: 60, discount_format: "40%", total_format: "60%"]

```

## discountPercent(percent, value, symbol){ ##
// Desconto ou inclusão por porcentagem
### Ex: ###

```
#!javascript

$tool.getDiscountPercent(10, $tool.moneyToFloat("R$ 60,00", "R$"), "R$");

// [discount_price: 6, total_price_with_discount: 54, total_price_with_increase: 66, discount_price_format: "R$ 6,00", total_price_with_discount_format: "R$ 54,00"…]

```


# Instalação GruntJS #

Instalar no PC o Nodejs

npm install -g grunt-cli

### Depois no projeto criar 2 aquivos ###

package.json


```
#!javascript

{
  "name": "nomedoProjeto",
  "version": "0.0.1",
  "description": "Descrição do projeto",
}

```

Gruntfile.js


```
#!javascript

module.exports = function(grunt){
 
   grunt.initConfig({
   	   pkg: grunt.file.readJSON('package.json'),
         uglify: {
           'e.tools.min.js': 'e.tools.js',
         }
   });

   //Carregar modulo
   grunt.loadNpmTasks('grunt-contrib-uglify'); // Minifica arquivos

   // Do
   grunt.registerTask('default',['uglify']);

}
```


### Fazer o download do grunt para o projeto ###

cd C:\wamp\www\WEB\Projetos git\plugins\etools

npm install grunt --save-dev

### plugins ###

npm install grunt-contrib-concat --save-dev

npm install grunt-contrib-uglify --save-dev

npm install grunt-contrib-imagemin --save-dev

### Se não existir uma pasta node_module, então é so digitar ###

npm install grunt

A pasta sera criada vinculando as dependencias do package.json

### Rodar Grunt ###

digite grunt